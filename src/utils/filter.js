// кастомная реализация метода массивов filter()

const array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

/**
 * @template T
 * @param {function(T): boolean} predicate
 * @param {Array<T>}
 * @returns {Array<T>}
 */

// eslint-disable-next-line no-extend-native
Array.prototype.filterCustom = function (predicate) {
  let i = 0;

  const collection = this.reduce((accumulator, element) => {
    if (predicate(this[i++], element, this)) {
      accumulator = [...accumulator, element];
    } else {
      accumulator = [...accumulator];
    }

    return accumulator;
  }, []);

  return collection;
};

const arrayMultipliedByTwo = array.filterCustom((number) => number > 4);

console.log(arrayMultipliedByTwo);
