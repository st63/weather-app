// кастомная реализация метода массивов map()

const array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

/**
 * @template T, V
 * @param {function(T): V} callback
 * @param {Array<T>}
 * @returns {Array<V>}
 */

// eslint-disable-next-line no-extend-native
Array.prototype.mapCustom = function (callback) {
  let i = 0;

  const collection = this.reduce((acc, elem) => [
    ...acc,
    (callback(this[i++], elem, this)),
  ], []);

  return collection;
};

const arrayMultipliedByTwo = array.mapCustom((number) => number * 3);

console.log(arrayMultipliedByTwo);
