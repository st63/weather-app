export type City = {
    name: 'Samara' | 'Toliatty' | 'Saratov' | 'Kazan' | 'Krasnodar'
    value: 'lat=53.19&lon=50.10' | 'lat=53.50&lon=49.42' | 'lat=51.53&lon=46.03' | 'lat=55.79&lon=49.10' | 'lat=45.03&lon=38.97'
}

export const CITIES: City[] = [
  { name: 'Samara', value: 'lat=53.19&lon=50.10' },
  { name: 'Toliatty', value: 'lat=53.50&lon=49.42' },
  { name: 'Saratov', value: 'lat=51.53&lon=46.03' },
  { name: 'Kazan', value: 'lat=55.79&lon=49.10' },
  { name: 'Krasnodar', value: 'lat=45.03&lon=38.97' },
];

export const APP_ID = '04160438ae6d577745ad287cda3d9bca';
